import React from 'react';
import { HashRouter, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import About from './pages/About';
import NotFound from "./pages/NotFound";

const App = () => {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" exact element={<Home/>} />
        <Route path="/a-propos" exact element={<About/>} />
        <Route element={<NotFound/>} />
      </Routes>
    </HashRouter>
  );
};

export default App;