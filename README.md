Node module: L'ensemble des bibliothèque de notre projet.

## BABEL

Installer preset pour convertir la syntaxe JSX

`npm install --save-dev @babel/preset-react`

Installer Flow ou TypeScript pour vérifier les types.

Flow:

`npm install --save-dev @babel/preset-flow`

Typescript:

`npm install --save-dev @babel/preset-typescript`

Installer les paquets:

`npm install --save-dev @babel/core @babel/cli @babel/preset-env`

Compiler tout votre code du répertoire src vers lib :

`./node_modules/.bin/babel src --out-dir lib`
or
`npx babel src --out-dir lib`

La fonctionnalité principale de Babel réside dans le module @babel/core.

`npm install --save-dev @babel/core`

@babel/cli est un outil qui vous permet d'utiliser babel depuis le terminal.

`npm install --save-dev @babel/core @babel/cli`




`npm i -D webpack webpack-dev-server cross-env`

Entrer les 2 scripts:

`"dev": "cross-env NODE_ENV=development webpack-dev-server --hot",
"buid": "cross-env NODE_ENV=production webpack"`

`npm i -D babel-loader babel-core babel-preset-es2015 babel-preset-react`

`npm run dev`