# WEBPACK

Webpack crée en interne un graphe de dépendances à partir d'un ou plusieurs points d'entrée , puis combine chaque module dont votre projet a besoin en un ou plusieurs bundles.

### Entry

Un point entry indique quel module webpack doit utiliser pour commencer à construire son graphe de dépendance interne 

ex: webpack.config.js

```
module.exports = {
  entry: './path/to/my/entry/file.js',
};
```

### Output

La propriété output indique à webpack où émettre les bundles qu'il crée et comment nommer ces fichiers.

ex: webpack.config.js

```
const path = require('path');

module.exports = {
  entry: './path/to/my/entry/file.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'my-first-webpack.bundle.js',
  },
};
```

### Loaders

Les loaders permettent à webpack de traiter d'autres types de fichiers et de les convertir en modules valides pouvant être consommés par votre application.

Installer les loaders dont nous avons besoin :
` npm install --save-dev (extension)-loader`

les loaders ont deux propriétés dans votre configuration Webpack :

1. `test` identifie le ou les fichiers à transformer.
2. `use` indique quel chargeur doit être utilisé pour effectuer la transformation.

ex: webpack.config.js

```
const path = require('path');

module.exports = {
  output: {
    filename: 'my-first-webpack.bundle.js',
  },
  module: {
    rules: [{ test: /\.txt$/, use: 'raw-loader' }],
  },
};
```
est équilent a dire
`
"Hey compilateur webpack, lorsque vous rencontrez un chemin qui se résout en un fichier '.txt' à l'intérieur d'une instruction require()/ import, utilisez le raw-loader pour le transformer avant de l'ajouter au bundle."
`

### Plugins

Les plugins peuvent être exploités pour effectuer large éventail de tâches telles que l'optimisation des bundles, la gestion des actifs et l'injection de variables d'environnement.

ex: webpack.config.js:

```
const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins

module.exports = {
  module: {
    rules: [{ test: /\.txt$/, use: 'raw-loader' }],
  },
  plugins: [new HtmlWebpackPlugin({ template: './src/index.html' })],
};
```
Dans l'exemple ci-dessus, le html-webpack-plugingénère un fichier HTML pour votre application et injecte automatiquement tous vos bundles générés dans ce fichier.

### Mode

En définissant le mode paramètre sur `development`, `production` ou `none`, vous pouvez activer les optimisations intégrées de webpack qui correspondent à chaque environnement. La valeur par défaut est `production`.

```
module.exports = {
  mode: 'production',
};
```

### Configuration

```
const path = require('path');

module.exports = {

  mode: "production", // "production" | "development" | "none"
  // Le mode choisi indique à webpack d'utiliser ses optimisations intégrées en conséquence.
  entry: "./app/entry", // string | object | array
  // defaut a ./src
  // Ici, l'application commence à s'exécuter et webpack commence à regrouper
  
  output: {
    // options liées à la façon dont webpack émet des résultats

    path:path.resolve(__dirname, "dist"), // string (default)
    // le répertoire cible pour tous les fichiers de sortie doit être un chemin absolu (utilisez le module de chemin Node.js)

    filename: "[name].js", // string (default)
    // le modèle de nom de fichier pour les morceaux d'entrée

    publicPath: "/assets/", // string
    // l'url du répertoire de sortie résolu par rapport à la page HTML
    library: { // Il existe également une ancienne syntaxe disponible pour cela
      type: "umd", // définition de module universel
      // le type de la bibliothèque exportée
      name: "MyLibrary", // string | string[]
      // le nom de la bibliothèque exportée

      /* Configuration avancée de output.library */
    },

    uniqueName: "my-application", // (defaults to package.json "name")
    // nom unique pour ce build pour éviter les conflits avec d'autres builds dans le même code HTML
    name: "my-config",
    // nom de la configuration, affiché en sortie
    /* Configuration de sortie avancée */
    /* Configuration de sortie expert 1 */
    /* Configuration de sortie expert 2 */
  },

  module: {
    // configuration concernant les modules
    rules: [
      // règles pour les modules (configure loaders, parser options, etc.)
      {
        // Conditions:
        test: /\.jsx?$/,
        include: [
          path.resolve(__dirname, "app")
        ],
        exclude: [
          path.resolve(__dirname, "app/demo-files")
        ],
        // ce sont des conditions de correspondance, chacun acceptant une expression régulière ou un test de chaîne et inclure ont le même comportement, les deux doivent être mis en correspondance exclure ne doit pas être mis en correspondance (prend la préférence sur test et inclut)
        // Les meilleures pratiques:
        // - Utilisez RegEx uniquement dans le test et pour la correspondance des noms de fichiers
        // - Utilisez des tableaux de chemins absolus dans les options d'inclusion et d'exclusion pour correspondre au chemin complet
        // - Essayez d'éviter d'exclure et préférez inclure
        // Chaque condition peut également recevoir un objet avec les propriétés "et", "ou" ou "pas" qui sont un tableau de conditions.
        issuer: /\.css$/,
        issuer: path.resolve(__dirname, "app"),
        issuer: { and: [ /\.css$/, path.resolve(__dirname, "app") ] },
        issuer: { or: [ /\.css$/, path.resolve(__dirname, "app") ] },
        issuer: { not: [ /\.css$/ ] },
        issuer: [ /\.css$/, path.resolve(__dirname, "app") ], // like "or"
        // conditions pour l'émetteur (origine de l'importation)
        /* Conditions avancées */

        // Actions:
        loader: "babel-loader",
        // le loader qui doit être appliqué, il sera résolu par rapport au contexte
        options: {
          presets: ["es2015"]
        },
        // options pour le loader
        use: [
          // appliquer plusieurs chargeurs et options à la place
          "htmllint-loader",
          {
            loader: "html-loader",
            options: {
              // ...
            }
          }
        ],
        type: "javascript/auto",
        // spécifie le type de module
        /* Actions avancées */
      },
      {
        oneOf: [
          // ... (rules)
        ]
        // n'utilisez qu'une de ces règles imbriquées
      },
      {
        // ... (conditions)
        rules: [
          // ... (rules)
        ]
        // utiliser toutes ces règles imbriquées (combiner avec des conditions pour être utile)
      },
    ],
    /* Configuration avancée du module */
  },
  resolve: {
    // options de résolution des demandes de module (ne s'applique pas à la résolution des chargeurs)
    modules: ["node_modules",path.resolve(__dirname, "app")],
    // répertoires où chercher les modules (dans l'ordre)
     extensions : [".js", ".json", ".jsx", ".css"],,
    // les extensions qui sont utilisées
    alias: {
      // une liste d'alias de nom de module
      // les alias sont importés par rapport au contexte courant
       "module": "nouveau-module",
      // alias "module" -> "new-module" and "module/path/file" -> "new-module/path/file"
      "only-module$": "new-module",
      // alias "only-module" -> "new-module", but not "only-module/path/file" -> "new-module/path/file"
      "module": path.resolve(__dirname, "app/third/module.js"),
      // alias "module" -> "./app/third/module.js" and "module/file" results in error
      "module": path.resolve(__dirname, "app/third"),
      // alias "module" -> "./app/third" and "module/file" -> "./app/third/file"
      [path.resolve(__dirname, "app/module.js")]: path.resolve(__dirname, "app/alternative-module.js"),
      // alias "./app/module.js" -> "./app/alternative-module.js"
    },
    /* Syntaxe d'alias alternative */
    /* Configuration de résolution avancée */
    /* Configuration de la résolution d'experts */
  },
  performance: {
    hints: "warning", // enum
    maxAssetSize: 200000, // int (in bytes),
    maxEntrypointSize: 400000, // int (in bytes)
    assetFilter: function(assetFilename) {
      // Prédicat de fonction qui fournit des noms de fichiers d'actifs
      return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
    }
  },
  devtool: "source-map", // enum
  // enhance debugging by adding meta info for the browser devtools
  // source-map most detailed at the expense of build speed.
  context: __dirname, // string (absolute path!)
  // the home directory for webpack
  // the entry and module.rules.loader option
  //   is resolved relative to this directory
  target: "web", // enum
  // the environment in which the bundle should run
  // changes chunk loading behavior, available external modules
  // and generated code style
  externals: ["react", /^@angular/],
  // Don't follow/bundle these modules, but request them at runtime from the environment
  externalsType: "var", // (defaults to output.library.type)
  // Type of externals, when not specified inline in externals
  externalsPresets: { /* ... */ },
  // presets of externals
  ignoreWarnings: [/warning/],
  stats: "errors-only",
  stats: {
    // lets you precisely control what bundle information gets displayed
    preset: "errors-only",
    // A stats preset

    /* Advanced global settings (click to show) */

    env: true,
    // include value of --env in the output
    outputPath: true,
    // include absolute output path in the output
    publicPath: true,
    // include public path in the output

    assets: true,
    // show list of assets in output
    /* Advanced assets settings (click to show) */

    entrypoints: true,
    // show entrypoints list
    chunkGroups: true,
    // show named chunk group list
    /* Advanced chunk group settings (click to show) */

    chunks: true,
    // show list of chunks in output
    /* Advanced chunk group settings (click to show) */

    modules: true,
    // show list of modules in output
    /* Advanced module settings (click to show) */
    /* Expert module settings (click to show) */

    /* Advanced optimization settings (click to show) */

    children: true,
    // show stats for child compilations

    logging: true,
    // show logging in output
    loggingDebug: /webpack/,
    // show debug type logging for some loggers
    loggingTrace: true,
    // show stack traces for warnings and errors in logging output

    warnings: true,
    // show warnings

    errors: true,
    // show errors
    errorDetails: true,
    // show details for errors
    errorStack: true,
    // show internal stack trace for errors
    moduleTrace: true,
    // show module trace for errors
    // (why was causing module referenced)

    builtAt: true,
    // show timestamp in summary
    errorsCount: true,
    // show errors count in summary
    warningsCount: true,
    // show warnings count in summary
    timings: true,
    // show build timing in summary
    version: true,
    // show webpack version in summary
    hash: true,
    // show build hash in summary
  },
  devServer: {
    proxy: { // proxy URLs to backend development server
      '/api': 'http://localhost:3000'
    },
    static: path.join(__dirname, 'public'), // boolean | string | array | object, static file location
    compress: true, // enable gzip compression
    historyApiFallback: true, // true for index.html upon 404, object for multiple paths
    hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
    https: false, // true for self-signed, object for cert authority
    // ...
  },
  experiments: {
    asyncWebAssembly: true,
    // WebAssembly as async module (Proposal)
    syncWebAssembly: true,
    // WebAssembly as sync module (deprecated)
    outputModule: true,
    // Allow to output ESM
    topLevelAwait: true,
    // Allow to use await on module evaluation (Proposal)
  },
  plugins: [
    // ...
  ],
  // list of additional plugins
  optimization: {
    chunkIds: "size",
    // method of generating ids for chunks
    moduleIds: "size",
    // method of generating ids for modules
    mangleExports: "size",
    // rename export names to shorter names
    minimize: true,
    // minimize the output files
    minimizer: [new CssMinimizer(), "..."],
    // minimizers to use for the output files

    /* Advanced optimizations (click to show) */

    splitChunks: {
      cacheGroups: {
        "my-name": {
          // define groups of modules with specific
          // caching behavior
          test: /\.sass$/,
          type: "css/mini-extract",

          /* Advanced selectors (click to show) */

          /* Advanced effects (click to show) */
        }
      },

      fallbackCacheGroup: { /* Advanced (click to show) */ }

      /* Advanced selectors (click to show) */

      /* Advanced effects (click to show) */

      /* Expert settings (click to show) */
    }
  },
  /* Advanced configuration (click to show) */
  /* Advanced caching configuration (click to show) */
  /* Advanced build configuration (click to show) */
}
```